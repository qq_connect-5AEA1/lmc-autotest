<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.24.js" type="text/javascript"></script>
    <link rel="stylesheet" media="screen" href="/content/mylogin/css/login.css">
    <title>BSF全链路自动化测试工具 - by 车江毅</title>
</head>
<body>
<div class="apTitle">BSF全链路自动化测试平台</div>
<div class="logcon">
    <input class="login-name" type="text" placeholder="请输入用户名">
    <input class="login-password" type="password" placeholder="请输入密码">
    <button class="login-button" type="button">登录</button>
</div>

<!-- particles.js container -->
<div id="particles-js" style="z-index:-1;display: flex;align-items: center;justify-content: center">
    <canvas class="particles-js-canvas-el" style="width: 100%; height: 100%;" width="472" height="625"></canvas>
</div>
</body>
<!-- scripts -->
<script src="/content/mylogin/js/login.js"></script>
<script src="/content/mylogin/js/loginApp.js"></script>
<script>
    $(function () {
        // 登录
        $(".login-button").click(function () {
            var name = $(".login-name").val();
            var password = $(".login-password").val();
            $.ajax({
                url: '/login/',
                type: "post",
                data: {
                    username: name,
                    password: password,
                },
                success: function (data) {
                    if (data.code === 200) {
                        window.location.href = "/task/index/";
                    } else {
                        alert(data.message)
                    }
                }
            });
        });
    });
</script>
</html>
