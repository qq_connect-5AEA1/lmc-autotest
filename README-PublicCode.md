# 公共方法库示例
公共方法库是es5语法js编写的通用函数库。

#### 公共方法库的案例模板
```
/*针对部署场景，域名映射处理*/
function MyReplaceUrl(dataMap){
	 var header = JSON.parse(dataMap["header"]);
      var appName = dataMap["app_name"];
	 var attribute = JSON.parse(dataMap["attribute"]);
	 var url = dataMap["url"];
	 var host = header["host"];
	 var env = null;
	 if(attribute!=null){
		 env=attribute["env"];
	 }
	 if(env!=null && env !=""){
	     /* /basicData  /pay/  /openApi/   /live/   /customer/  /operation/  /platform/ 针对自身实际情况调整*/
		  var domain= {'lmc-platform-provider':'/platform','lmc-pay-provider':'/pay','lmc-openApi-provider':'/openApi',
		  'lmc-live-provider':'/live', 'lmc-customer-provider':'/customer','lmc-operation-provider':'/operation',
		  'lmc-basic-data-provider':'/basicData'};
		  /*域名映射,解决k8s内网访问问题；重写url host和端口,指向k8s内部负载均衡节点*/
		  for(var key in domain){
			  if(appName==key){
					 return url.replace(host,env+"-gateway.linkmore.com"+domain[key]).replace("http:","https:");/*替换域名,走网关*/
			   }
		  }
	 }
	 return url;
}
/*扩展api动态参数获取支持默认值*/
function MyApiPsParams(key,defaultValue){
    if(api.ps.params[key]!=null){
       return api.ps.params[key];/*动态参数*/
    }
    return defaultValue;
}
/*格式化时间:yyyy_MM_dd*/
function MyDateFormat(dat){
    var year = dat.getFullYear();
    var mon = (dat.getMonth()+1) < 10 ? "0"+(dat.getMonth()+1) : dat.getMonth()+1;
    var data = dat.getDate()  < 10 ? "0"+(dat.getDate()) : dat.getDate();
    var newDate = year +"_"+ mon +"_"+ data;
    return newDate;
}
/*时间日期累加*/
function MyAddDays(date,add) {
    date.setDate(date.getDate()+add);
    return date;
}
/*url 获取相对路径*/
function MyUrlRelativePath(url)
{
　　var ss = url.split("//");
   if(ss.length!=2)
      return url;/*不合规*/
    var url2=ss[1];
　　var start = url2.indexOf("/");
    if(start<=-1){
      return "";/*无path */
    }
　　var relUrl = url2.substring(start);
    /*无query*/
　　if(relUrl.indexOf("?") != -1){
　　　　relUrl = relUrl.split("?")[0];
　　}
　　return relUrl;
}
/*根据url path去重,取path集合*/
function MyDistinctUrlPath(urls){
    var distincts=[];
    for(var i=0;i<urls.length;i++){
        var path = MyUrlRelativePath(urls[i]);
        if(distincts.indexOf(path)<=-1){
            distincts.push(path);
        }
    }
    return distincts;
}
/*消息通知,目前通知飞书*/
function MyNotify(env,content,reportid){
    var token="8e8c2031-18d0-4cf4-9ff1-a8f238ee7b55";
    if(env==null||env=="test"||env=="dev"){
        /*token="";*/
    }
    if(content!=null&&env!=null){
        content+=" 环境:"+env;
    }
    if(content!=null&&reportid!=null){
        content+=" 报告地址:http://{域名}/report/view/?id="+reportid;
    }
    api.httpPost("https://open.feishu.cn/open-apis/bot/v2/hook/"+token,{"msg_type":"text","content":{"text":content}});
}
   ```
公共方法库仅供参考,实际根据自身场景编写公共方法！一般公共函数要有特殊开头，便于识别。比如我们会以My开头~
示例：
   ```
    /*通过sql流处理分批获取样本数据，然后写入到本地样本(sample)文件中*/
    var env= MyApiPsParams('env','pre');
    var date = MyApiPsParams('date',api.nowFormat("2023_02_07"));
    api.streamSql2("select * from auto_tb_sample_2023_02_07 "+" where  attribute->'$.env'='"+env+"' and attribute->'$.apiType'='查询' and attribute->'$.test'='需要' and operator_type='仅查询' order by id desc limit 2000",[],function (dataMap){
          dataMap["url"]=MyReplaceUrl(dataMap);/*公共库函数执行*/
          api.writeSample(dataMap);
    })
    /*压测时,定期心跳检测当前任务是否符合退出条件，进行任务退出关闭动作，一般会根据压测报告结果或者运行时间进行判断*/
    /*此处举例吞吐量超过5000,运行时间超过【10】分钟则终止任务,特别注意nodeReport在任务刚启动的时候可能为null*/
    if(api.ps.nodeReport!=null&&(api.ps.nodeReport.throughput>5000||api.ps.runtime>10*60)){
    /*飞书通知*/
    var content = "定时任务通知:【qps】全链路接口压测报告，压测已结束!";
    MyNotify(api.ps.params["env"],content,api.ps.report.id);
    api.log(["结束",api.ps.nodeReport,api.ps.runtime]);
    return false;
}
   ```

by [车江毅](https://www.cnblogs.com/chejiangyi/)